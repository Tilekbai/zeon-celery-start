from celery import Celery
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "celery_source.settings")
app = Celery("celery_source")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()