from celery import Celery
import re

app = Celery('swear_check', broker='redis://localhost:6379')  # Replace with your own broker URL


@app.task
def check_for_swear_words(text):
    swear_words = ['fuck', 'whole', 'bitch']  # Add your list of swear words here

    # Check if any swear word is present in the text
    for word in swear_words:
        if re.search(r'\b' + re.escape(word) + r'\b', text, re.IGNORECASE):
            print("<<<<<<<<<<  ..... MAT .....  >>>>>>>>>>>")
            print("Swear words: ", text)
            return True
        else:
            print("There are no swear words!")
    return False
