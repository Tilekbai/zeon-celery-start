from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from feedback.forms import FeedbackForm


class FeedbackFormView(FormView):
    template_name = "feedback/feedback.html"
    form_class = FeedbackForm
    success_url = "/success/"

    def form_valid(self, form):
        form.send_email()
        return super().form_valid(form)
    


class SuccessView(TemplateView):
    template_name = "feedback/success.html"
 

# from .swear_check import check_for_swear_words

# def process_text(text):
#     result = check_for_swear_words.delay(text)
    # Do other processing or return the result directly

# Example usage
# text = "This is a text that may contain swear words: for example bitch"
# process_text(text)
