from time import sleep
from django.core.mail import send_mail
from celery import shared_task

# from .views import process_text

from .swear_check import check_for_swear_words



@shared_task    
def send_feedback_email_task(email_address, message):
    """Sends an email when the feedback form has been submitted."""
    sleep(20)  # Simulate expensive operation(s) that freeze Django
    send_mail(
        "Your Feedback",
        f"\t{message}\n\nThank you!",
        ["support@example.com"],
        [email_address],
        fail_silently=False,
    )
    
    def process_text(text):
        result = check_for_swear_words.delay(text)
    text = message
    process_text(text)

